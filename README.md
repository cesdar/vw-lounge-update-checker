# VW Lounge Update Checker

Ein kleines Tool, das regelmäßig die große und kleine Cars JSON Daten aus der VW Lounge abruft und einen bei Änderungen informiert. Zumindest ist das der Plan. Aktuell kann es nur sowohl die kleine als auch die große cars datei auslesen. Wenn du nicht weißt, worum es genau geht: Dann hier: https://www.goingelectric.de/forum/viewtopic.php?f=388&t=60969

Ein Dank geht auch an den Hinweis sich direkt cURL abfragen in Chrome ausgeben zu lassen wie u.a. hier von Houwelk beschrieben: https://www.goingelectric.de/forum/viewtopic.php?f=388&t=51615&start=16820