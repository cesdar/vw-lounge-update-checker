import requests
import json
from auth import bearer
from auth import sessionCookie
from auth import csrfToken

verbose = False

#requestHeaders = {'authorization':'Baerer ' + baerer}

#bigCarsRequest = requests.get("https://myvwde.cloud.wholesaleservices.de/api/waiting-lounge/cars",requestHeaders)
'''
heads = {
    'Host':'identity.vwgroup.io',
    'Connection':'keep-alive'
}
'''
'''
heads ={
    'Host': 'identity.vwgroup.io',
    'Connection': 'keep-alive',
    'Upgrade-Insecure-Requests': '1',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
    'Sec-Fetch-Site': 'cross-site',
    'Sec-Fetch-Mode': 'navigate',
    'Sec-Fetch-User': '?1',
    'Sec-Fetch-Dest': 'document',
    'sec-ch-ua': '"Google Chrome";v="89", "Chromium";v="89", ";Not A Brand";v="99"',
    'sec-ch-ua-mobile': '?0',
    'Referer': 'https://www.volkswagen.de/',
    'Accept-Encoding': 'gzip, deflate, br',
    'Accept-Language': 'de-DE,de;q=0.9'
}

url = "https://identity.vwgroup.io/oidc/v1/authorize?response_type=code&client_id=4fb52a96-2ba3-4f99-a3fc-583bb197684b@apps_vw-dilab_com&scope=profession%20cars%20address%20phone%20openid%20profile%20dealers%20vin%20carConfigurations&redirect_uri=https://www.volkswagen.de/app/authproxy/login/oauth2/code/vw-de"

req = requests.get(url, headers = heads)

print(req.cookies)
print(req.headers)
print(req.status_code)

print('-----------------------------')

url = 'https://www.volkswagen.de/app/authproxy/login?fag=vw-de,vw-ag-direct-sales,vwag-weconnect&scope-vw-de=profile,address,phone,carConfigurations,dealers,cars,vin,profession&scope-vw-ag-direct-sales=address,phone,profile&scope-vwag-weconnect=openid,mbb&prompt-vw-de=login&prompt-vw-ag-direct-sales=none&prompt-vwag-weconnect=none&redirectUrl=https://www.volkswagen.de/de.html'

req = requests.session()
response = req.get(url)

cookies = req.cookies

print(response.status_code)
print(req.headers)
print(cookies)
'''

# Get Tokens

getTokensURL = 'https://www.volkswagen.de/app/authproxy/vw-de/tokens'
getTokensHeaders = {
  'authority' : 'www.volkswagen.de',
  'sec-ch-ua' : '"Google Chrome";v="89", "Chromium";v="89", ";Not A Brand";v="99"',
  'accept' : 'application/json, text/plain, */*',
  'x-csrf-token' : csrfToken,
  'sec-ch-ua-mobile' : '?0',
  'user-agent' : 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36',
  'sec-fetch-site' : 'same-origin',
  'sec-fetch-mode' : 'cors',
  'sec-fetch-dest' : 'empty',
  'referer' : 'https://www.volkswagen.de/de/besitzer-und-nutzer/myvolkswagen.html?---=%7B%22myvw_garage-preview%22%3A%22%2Ftile%2Fpreview%22%2C%22myvw_garage-add%22%3A%22%2Ftile%2Fadd%22%2C%22myvw_user-preview%22%3A%22%2Ftile%2Fpreview%22%2C%22myvw_medialibrary-preview%22%3A%22%2Ftile%2Fpreview%22%2C%22myvw_servicevoucher-default%22%3A%22%2Ftile%22%2C%22myvw_waitinglounge-default%22%3A%22%2Ftile%22%2C%22myvw_tbo-default%22%3A%22%2Ftile%22%2C%22CustomerInteractionCenter-default%22%3A%22%2Ftile%22%2C%22myvw_offer-default%22%3A%22%2Ftile%22%2C%22myvw_weMapUpdate-default%22%3A%22%2Ftile%22%2C%22myvw_cashback-default%22%3A%22%2Ftile%22%2C%22myvw_adventureplanner-default%22%3A%22%2Ftile%22%2C%22myvw_financialservicesag-default%22%3A%22%2Ftile%22%2C%22myvw_gmd-default%22%3A%22%2Ftile%22%2C%22besitzer-und-nutzer_myvolkswagen_featureAppSection%22%3A%22%2Ftiles%22%7D',
  'accept-language' : 'de-DE,de;q=0.9,en-US;q=0.8,en;q=0.7',
  'cookie' : sessionCookie
}

getTokensRequest = requests.get(getTokensURL, headers=getTokensHeaders)
if getTokensRequest.status_code == 200:
    bearer = getTokensRequest.json()["access_token"]
    print("got Tokens")
    if verbose: print("Token: ", bearer)
else:
    print("got no Token. Using default token in auth.py")

# Token processing

bearerFull = "Bearer " + bearer

# Get small Cars json file
smallCarsURL = 'https://myvwde.cloud.wholesaleservices.de/api/waiting-lounge/cars'

smallCarsHeaders = {
  'authority' : 'myvwde.cloud.wholesaleservices.de',
  'sec-ch-ua' : '"Google Chrome";v="89", "Chromium";v="89", ";Not A Brand";v="99"',
  'authorization' : bearerFull,
  'sec-ch-ua-mobile' : '?0',
  'user-agent' : 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36',
  'content-type' : 'application/json',
  'accept' : '*/*',
  'origin' : 'https://www.volkswagen.de',
  'sec-fetch-site' : 'cross-site',
  'sec-fetch-mode' : 'cors',
  'sec-fetch-dest' : 'empty',
  'referer' : 'https://www.volkswagen.de/',
  'accept-language' : 'de-DE,de;q=0.9,en-US;q=0.8,en;q=0.7' 
}

smallCarsRequest = requests.get(smallCarsURL, headers=smallCarsHeaders)
print("Small Cars Status Code: ", smallCarsRequest.status_code)
if smallCarsRequest.status_code == 200:
    if verbose: print(smallCarsRequest.json())
    smallCarsJson = smallCarsRequest.json()[0]
else:
    smallCarsJson = {'name': 'N/A', 'orderStatus': 'N/A', 'deliveryDateType': 'N/A', 'deliveryDateValue': 'N/A'}

# Get big Cars json file

bigCarsHeaders ={
    'authority' : 'myvwde.cloud.wholesaleservices.de',
    'authorization' : bearerFull,
    'user-agent' : 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.182 Safari/537.36',
    'content-type' : 'application/json',
    'accept' : '*/*',
    'origin' : 'https://www.volkswagen.de',
    'sec-fetch-site' : 'cross-site',
    'sec-fetch-mode' : 'cors',
    'sec-fetch-dest' : 'empty',
    'referer' : 'https://www.volkswagen.de/',
    'accept-language' : 'de,en;q=0.9,en-US;q=0.8'
}

bigCars = requests.get('https://w1hub-backend-production.apps.emea.vwapps.io/cars', headers=bigCarsHeaders)
print("Big cars status code:\t",bigCars.status_code)
if bigCars.status_code == 200:
    if verbose: print(bigCars.json())
    bigCarsJson = bigCars.json()[0]
else:
    bigCarsJson = {
    'commissionNumber': 'N/A', 
    'vin': None, 
    'deliveryDate': 'N/A', 
    'orderStatus': 'N/A', 
    'name': 'N/A', 
    'brand': 'N/A', 
    'modelCode': 'N/A', 
    'modelYear': 'N/A', 
    'orderDate': 'N/A', 
    'checkpointNumber': 'N/A', 
    'detailStatus': 'N/A'
    }

print("-----------------")
print("Name:\t\t\t", smallCarsJson["name"])
print("Status:\t\t\t",smallCarsJson["orderStatus"])
print("Lounge Deliverydare:\t", smallCarsJson["deliveryDateValue"])
print("Commission Number:\t", bigCarsJson["commissionNumber"])
print("VIN:\t\t\t", bigCarsJson["vin"])
print("Delivery Date:\t\t",bigCarsJson["deliveryDate"])
print("Order Status:\t\t",bigCarsJson["orderStatus"])
print("Checkpoint:\t\t",bigCarsJson["checkpointNumber"], ": ", bigCarsJson["detailStatus"])