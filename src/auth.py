'''
Put here your SESSION cookie and csrf token.
    - Log In to the VW Lounge and go to the "Dashboard". 
    - In Chrome press Ctrl+Shift+I. 
    - Navigate to the "Network" tab.
    - search for "tokens" and select the corresponing entry
    - Under request header search for "coockie:"
    - right click on the row and choose "Copy value"
    - Copy this value in the parantheses after sessionCookie
    - do the same with "x-csrf-token"
'''

sessionCookie = "replaceWithHeaderCookieValue"
csrfToken = "replaceWithCsrfToken"

'''
Put here your baerer token, that was sent with the cars request. 
You can retrieve it from the "Request Headers" in the "Headers Tab" next to the response tab at the cars request.
This is only used as fallback if the session cookie is not set.
'''

bearer = "replaceWithYourToken"

'''
Room for your credentials
'''

mail = "your@mail.de"
passwd = "YourSuperSecretPassword12345"
